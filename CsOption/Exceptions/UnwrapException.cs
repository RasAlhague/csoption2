
using System;
using System.Runtime.Serialization;

namespace Options.Exceptions
{

    [Serializable]
    public class UnwrapException : Exception
    {
        public UnwrapException() { }
        public UnwrapException(string message) : base(message) { }
        public UnwrapException(string message, Exception inner) : base(message, inner) { }
        protected UnwrapException(
          SerializationInfo info,
          StreamingContext context) : base(info, context) { }
    }
}
