namespace Options
{
    /// <summary>
    /// Dient dazu einen Wert zu speichern. Some ist immer dann wenn Option einen wert speichern kann.
    /// </summary>
    /// <typeparam name="T">Jeder beliebige typ.</typeparam>
    public sealed class Some<T> : Option<T>
    {
        /// <summary>
        /// Daten die in der Option Variante gespeichert werden, wenn Option einen wert hat. 
        /// Wer hier ein null rein packt der ist vollkommen bedeppert und hat den Grund für Option nicht verstanden.
        /// </summary>
        public T Content { get; }

        /// <summary>
        /// Konstruktor der einen beliebigen Wert vom Typ T annehmen kann. 
        /// </summary>
        /// <exception cref="System.ArgumentException">Wird geworfen wenn versucht wird <see cref="null"/> zu übergeben.</exception>
        /// <param name="value"></param>
        /// <remarks>
        /// Für den fall das man einen potentielen null wert erwartet gibt es <see cref="Extensions.ObjectExtensions.NoneIfNull{T}(T)"/>. 
        /// </remarks>
        public Some(T value)
        {
            if(value == null)
            {
                throw new System.ArgumentException("Some can not hold to null. Please use None in case of null.");
            }
            this.Content = value;
        }
    }
}
