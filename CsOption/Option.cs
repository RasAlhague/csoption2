
namespace Options
{
    /// <summary>
    /// Basis für den Option Typ in C#. Überlädt den impliziten Konvertierungsoperator um das schreiben von Code zu vereinfachen.
    /// Sollte mit jedem beliebigen Typ funktionieren
    /// </summary>
    /// <typeparam name="T">Jeder beliebige Typ.</typeparam>
    public abstract class Option<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public static implicit operator Option<T>(T value) =>
            new Some<T>(value);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="none"></param>
        public static implicit operator Option<T>(None none) =>
            new None<T>();

        /// <summary>
        /// Prüft ob die Instanz vom Typ <see cref="Some{T}"/> ist.
        /// </summary>
        /// <returns>Gibt true zurück wenn die Instanz Some ist ansonsten false.</returns>
        public bool IsSome()
        {
            return this is Some<T>;
        }

        /// <summary>
        /// Prüft ob die Instanz vom Typ <see cref="None{T}"/> ist.
        /// </summary>
        /// <returns>Gibt true zurück wenn die Instanz None ist ansonsten false.</returns>
        public bool IsNone()
        {
            // Hier wird extra auf None<T> geprüft und nicht auf None, weil None nur ein Pseudowert ist zum vereinfachen der Syntax.
            return this is None<T>;
        }
    }
}
