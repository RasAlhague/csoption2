
using Options.Exceptions;
using System;

namespace Options.Extensions
{
    /// <summary>
    /// Extensions zum direkten entpacken von <see cref="Option{T}"/>. 
    /// Mit Vorsicht zu benutzen da einige zu <see cref="UnwrapException"/> führen können, was
    /// den sinn eines Option zerstören würde.
    /// </summary>
    public static class UnwrapExtensions
    {
        /// <summary>
        /// Extension fürs entpacken von <see cref="Option{T}"/>. Wirft bei None eine Exception.
        /// </summary>
        /// <typeparam name="T">Jeder beliebige Typ.</typeparam>
        /// <param name="val">Option value welches entpackt werden soll.</param>
        /// <exception cref="UnwrapException">Sollte es sich um ein None handeln wird eine <see cref="UnwrapException"/> geworfen.</exception>
        /// <returns>Gibt den Wert aus dem Some zurück.</returns>
        public static T Unwrap<T>(this Option<T> val)
        {
            return val.UnwrapOrElse(() => throw new UnwrapException("Option is none."));
        }

        /// <summary>
        /// Extension fürs entpacken von <see cref="Option{T}"/>. Führt im falle eines Nones einen fallback Code aus.
        /// </summary>
        /// <typeparam name="T">Jeder beliebige Typ.</typeparam>
        /// <param name="val">Option value welches entpackt werden soll.</param>
        /// <param name="predicate">Operation die im falle eines None ausgeführt wird.</param>
        /// <returns>Gibt den Wert aus dem Some zurück oder einen durch <paramref name="predicate"/> entstandenen wert.</returns>
        public static T UnwrapOrElse<T>(this Option<T> val, Func<T> predicate)
        {
            if (val.IsSome())
            {
                var some = val as Some<T>;
                return some.Content;
            }
            else
            {
                return predicate();
            }
        }
    }
}
