
namespace Options
{
    /// <summary>
    /// Leere Klasse. Klasse ist gezieht leer, da None nichts hat.
    /// </summary>
    /// <typeparam name="T">Egal, None ist immer nichts</typeparam>
    public sealed class None<T> : Option<T>
    {
    }

    /// <summary>
    /// Singleton von None für den überladenen implicit Operator in Option.
    /// Dient der vereinfachung der Schreibweise.
    /// </summary>
    public sealed class None
    {
        /// <summary>
        /// Referenz auf die einzige None Instanz im Programm.
        /// </summary>
        public static None Value { get; } = new None();

        /// <summary>
        /// Privat um ein Singleton daraus zu machen.
        /// </summary>
        private None() { }
    }
}
